<?php
    session_start();
    if(isset($_GET['acao']) && $_GET['acao'] == "sair"){
        if($_SESSION['logado']){
            session_destroy();
            setcookie("sim", null);
            $erro = array("code"=>1);
            echo json_encode($erro);
        }
    }

?>