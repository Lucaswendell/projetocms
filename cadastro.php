<?php session_start(); 
    if(!isset($_SESSION['logado'])){
        require_once("menu/menu.php");
?>
        <div id="form_login">
            <form action="cadastrarUser.php" method="post">
            <p id="p_login" class="uk-text-center">Cadastro</p>
            <p id="alerta"></p>
            <div class="uk-padding-small">
                <label for="usuario">Identificador do usuario</label><br /><br />
                <div class="uk-inline uk-width-1">
                    <span class="uk-form-icon" uk-icon="icon: user; ratio: 1.5;"></span>
                    <input type="text" name="user" id="user" onblur="usuario(this);" placeholder="Usuario" class="uk-input">
                </div>
                <span id="erroU" class="erroSp"></span>
                <br /><br />
                <label for="senha">Senha:</label>
                <ul>
                    <li>A senha deve conter ao menos 8 caracteres.</li>
                    <li>Ao menos 1 letra(s) maiúscula(s).</li> 
                    <li>Ao menos 1 letra(s) minúscula(s).</li>
                    <li>Ao menos 1 digito(s).</li>
                    <li><em>obs.: Não inicie a senha com número.</em></li>
                </ul>
                <div class="uk-inline uk-width-1">
                    <a href="#" id="pass0" class="uk-form-icon" uk-icon="icon: lock; ratio: 1.5;"></a>
                    <input type="password" name="senha" onblur="senhaU(this);" id="pass" placeholder="Exemplo5" class="uk-input">
                </div>
                <span id="erroS" class="erroSp"></span>
                <br /><br />
                <label for="senha">Endereço de email:</label><br /><br />
                <div class="uk-inline uk-width-1">
                    <span class="uk-form-icon" uk-icon="icon: mail; ratio: 1.5;"></span>
                    <input type="email" name="email" onblur="emailU(this);" id="em" placeholder="exemplo@exemplo.com" class="uk-input">
                </div>
                <span id="erroEm" class="erroSp"></span>
                <br /><br />
                <input type="submit" value="cadastrar" class="uk-button uk-button-primary" id="bot"/>
                </div>
            </form>
        </div>
    </div>
    <?php
        if(isset($_SESSION['alerta']['errada']) ){
            echo 
            "
    <script>
        swal({
            text: \"{$_SESSION['alerta']['errada']}\",
            type: \"error\"
        });
    </script>
    ";
    unset($_SESSION['alerta']['errada']);
        }
    
    ?>
<?php require_once("menu/rodape.php");
}else{
    header("Location: index.php");
}?>