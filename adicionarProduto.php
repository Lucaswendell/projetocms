<?php
require("classes/model/Produto.class.php");
require_once("classes/model/Video.class.php");
require_once("classes/model/Foto.class.php");
require("classes/control/ProdutoControl.class.php");
require_once("classes/control/FotoControl.class.php");
require_once("classes/control/VideoControl.class.php");    
session_start();
    if(isset($_SESSION['logado']) && $_SESSION['adm']){
            $prod = new Produto();
            $prodCon = new ControlProd();
            $foto = new Foto();
            $fotoM = new FotoM();
            $video = new Video();
            $videoM = new VideoM();
            $fotoM->setNome($_POST['nome_p']);
            $videoM->setNome($_POST['nome_p']);
            $prod->setNome_produto($_POST['nome_p']);
            $prod->setValor($_POST['valor']);
            $prod->setCategoria($_POST['categoria']);   
            $prod->setDescricao($_POST['descricao']);
            $videoM->setTipo($_FILES["video"]["type"]);
            $fotoM->setTipo($_FILES['foto_p']['type']);
            if(!$fotoM->setFoto($_FILES['foto_p']['tmp_name'])){
                $erro = array("codigo"=>0, "mensagem"=>"Coleque uma imagem valida!(PNG,JPG, JPEG)");
                echo json_encode($erro); //retorna um json com a mensagem
                exit();
            }
            if(!$videoM->setVideo($_FILES['video']['tmp_name'])){
                $erro = array("codigo"=>0,"mensagem"=>"Coloque um video valido!(MP4, OGG)");
                echo json_encode($erro);
                exit();
            }
            if(!$prodCon->verificaProd($prod->getNome_produto())){
                if($prodCon->adicionaProd($prod)){
                    if($foto->adicionaFoto($fotoM)){
                        if($videoM->getVideo() != "vazio"){
                            if($video->adicionaVideo($videoM)){
                                $erro = array("codigo"=>1,"mensagem"=>"Produto adicionado com sucesso!");
                                echo json_encode($erro); //retorna um json com a mensagem  
                                exit();
                            }else{
                                $erro = array("codigo"=>0,"mensagem"=>"erro ao adicionar video");
                                echo json_encode($erro); //retorna um json com a mensagem  
                                exit();
                            }
                        }else{
                            $erro = array("codigo"=>1,"mensagem"=>"Produto adicionado com sucesso!");
                            echo json_encode($erro); //retorna um json com a mensagem  
                            exit();
                        }
                        
                    }else{
                        $erro = array("codigo"=>0, "mensagem"=>"tivemos um problema ao adicionar a imagem ou video");
                        echo json_encode($erro); //retorna um json com a mensagem  
                        exit();
                    }
                }else{
                    $erro = array("codigo"=>0, "mensagem"=>"tivemos um problema para adicionar o produto");
                    echo json_encode($erro); //retorna um json com a mensagem  
                    exit();
                }
            }else{
                $erro = array("codigo"=>0, "mensagem"=>"Produto já cadastrado");
                echo json_encode($erro); //retorna um json com a mensagem  
                exit();
            }  
    }else{
        header("Location: ./index.php");
    }


?>