<?php
require_once("classes/control/UserControl.class.php");
require_once("classes/model/User.class.php");
require_once("classes/control/FotoControl.class.php");
require_once("classes/model/Foto.class.php");
session_start();
$user = new User();
$userCont = new UserControl();
$user->setUsuario($_POST['user']);
$user->setSenha($_POST['senha']);
$user->setEmail($_POST['email']);
$user->setId($_GET['id']);
$fotoM = new FotoM();
$fotoC = new Foto();
$fotoM->setTipo($_FILES['foto_p']['type']);
if (!$fotoM->setFoto($_FILES['foto_p']['tmp_name'])) {
    $erro = array("codigo" => 0, "mensagem" => "Coloque uma imagem valida (JPEG, JPG, PNG)");
    echo json_encode($erro);
    exit();
} else {
    $fotoM->setNome($_POST['user']);
}
if ($user->getEmail() != $_SESSION['dadosUser']['email']) {
    if (!$userCont->checkEmail($user->getEmail())) {
        if ($userCont->updateUser($user)) {
            if ($fotoC->adicionaFotoUser($fotoM, $user->getId())) {
                $erro = array("codigo" => 1, "mensagem" => "Alteração feito com sucesso");
                echo json_encode($erro);
                exit();
            } else {
                $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
                echo json_encode($erro);
                exit();
            }
        } else {
            $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
            echo json_encode($erro);
            exit();
        }
    } else {
        $erro = array("codigo" => 0, "mensagem" => "Email já cadastrado");
        echo json_encode($erro);
        exit();
    }
} else if ($user->getUsuario() != $_SESSION['dadosUser']['usuario']) {
    if (!$userCont->checkUser($user->getUsuario())) {
        if ($userCont->updateUser($user)) {
            if ($fotoC->adicionaFotoUser($fotoM, $user->getId())) {
                $erro = array("codigo" => 1, "mensagem" => "Alteração feito com sucesso");
                echo json_encode($erro);
                exit();
            } else {
                $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
                echo json_encode($erro);
                exit();
            }
        } else {
            $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
            echo json_encode($erro);
            exit();
        }
    } else {
        $erro = array("codigo" => 0, "mensagem" => "Usuário já cadastrado");
        echo json_encode($erro);
        exit();
    }
} else {
    if ($userCont->updateUser($user)) {
        if ($fotoC->adicionaFotoUser($fotoM, $user->getId())) {
            $erro = array("codigo" => 1, "mensagem" => "Alteração feito com sucesso");
            echo json_encode($erro);
            exit();
        } else {
            $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
            echo json_encode($erro);
            exit();
        }
    } else {
        $erro = array("codigo" => 0, "mensagem" => "Erro inesperado");
        echo json_encode($erro);
        exit();
    }
}
?>