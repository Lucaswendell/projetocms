<?php
require_once("classes/control/ProdutoControl.class.php");
require_once("classes/control/VideoControl.class.php");
session_start();
require_once("menu/menu.php");
$pro = new ControlProd();
$consu = $pro->pegaProd($_GET['id']);
$video = new Video();
if ($consu) {
    ?>
    <div id="detalhes" class="uk-margin" uk-grid>
        <div class="produto_detalhe_foto uk-width-1-2@m" id="<? echo $_GET['id'] ?>">
            <?php 
            echo "<img src=\"image.php?id={$_GET['id']}\" onmouseover=\"zoom({$_GET['id']})\" />"
            ?>
        </div>
        <div class="produto_detalhes uk-width-1-2@m">
            <h3><?php echo $consu->getNome_produto() ?></h3>
            <p id="valor">R$ <?php echo number_format($consu->getValor(), 2, ",", "."); ?></p>
            <p>Categoria: <?php echo $consu->getCategoria(); ?></p>
            <p>
            <?php if (isset($_SESSION['adm']) && $_SESSION['adm']) { ?>  
                <a href="#" onclick="temCerteza(this,'<?php echo $consu->getId() ?>', '<?php echo $consu->getNome_produto() ?>', 'detalhes');" uk-icon="icon: trash; ratio:1.5;" uk-tooltip="excluir"></a>
                <a href="editar.php?id=<?php echo $consu->getId() ?>&pag=detalhes" uk-icon="icon: pencil; ratio:1.5;" uk-tooltip="editar"></a>
            <?php 
            } ?>
                <a class="#" uk-tooltip="adicionar ao carrinho"  onclick="carrinho(<?php echo $_GET['id'] ?>,this)" href="#" uk-icon="icon: cart; ratio: 1.5;"></a>
                <a class="uk-button uk-button-default" href="index.php">Ver outros produtos</a>
            </p>
        </div>
        <div class="produto_detalhes uk-width-1-1@m">
            <h2>Descrição</h2>
            <?php
            echo "{$consu->getDescricao()}";
            if ($video->verificaSeTem($consu->getId())) {
                echo "
                    <video controls>  
                        <source src=\"video.php?id={$_GET['id']}\" />
                    </video>";
            }
            ?>
     </div>
    </div>
<?php
} else {
    echo "<h1 class=\"uk-text-center\">Produto não encontrado</h1>";
}
require_once("menu/rodape.php");
echo "<script src=\"js/jquery.zoom.min.js\" ></script>
    <script>
        $(\"#{$_GET['id']}\").zoom()
    </script>";
?>
