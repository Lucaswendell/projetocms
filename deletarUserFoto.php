<?php
session_start();
if(isset($_SESSION['logado']) && $_SESSION['logado']){
    require_once("classes/control/FotoControl.class.php");
    $foto = new Foto();
    if($foto->deletarFotoUser($_GET['id'])){
        $erro = array("codigo"=>1, "mensagem"=>"Imagem deletada com sucesso");
        echo json_encode($erro);
        exit();
    }else{
        $erro = array("codigo"=>0, "mensagem"=>"Erro ao excluir imagem");
        echo json_encode($erro);
        exit(); 
    }
}else{
    header("Location: index.php");
}

?>