<?php
session_start();
    if(isset($_SESSION['logado']) && $_SESSION['logado'] ){
        require_once("menu/menu.php");
        $produto = new ControlProd();
        $produtos = array_keys($_SESSION['carrinho']);
        $total = 0;
        echo "
        <div id=\"tudo\">
        <div uk-overflow-auto>
        <table class=\"uk-table uk-table-divider\"  id=\"tableCa\">
        <thead>
            <tr>
                <th>Produto</th>
                <th>Quantidade</th>
                <th>Valor unitário</th>
                <th>Subtotal</th>
            </tr>
        </thead>
            <form action=\"?acao=enviar\" method=\"post\">        
        <tfoot>
            <tr><td><input type=\"submit\" value=\"confirmar compra\" class=\"uk-button uk-button-default\"></td></tr>
        </tfoot>
            <tbody>
        ";
    
        foreach ($produtos as $prod) {
            $pega = $produto->pegaProd($prod);
            $value = $_SESSION['carrinho'][$prod] * $pega->getValor();
            echo "
            <tr style=\"padding: 10px;\">
                <td>{$pega->getNome_produto()}</td>
                <td>{$_SESSION['carrinho'][$prod]}</td>
                <td>R$ " . number_format($pega->getValor(), 2, ",", ".") . "</td>
                <td>R$ " . number_format($value, "2", ",", ".") . "</td>
                <td><a href=\"?acao=remover&id={$pega->getId()}\" uk-tooltip=\"remover\" uk-icon=\"icon: trash; ratio:1.5;\"></a></td>
            </tr>";
            $total += $value;
        }
        echo "
            </tbody>
            </form>
         </table>
         </div>
         </div>";
         require_once("menu/rodape.php");
         if(isset($_GET['acao']) && $_GET['acao'] == "enviar"){
            echo "<script>swal({
                title:\"Sua compra foi efetivada\", 
                text:\"enviaremos um e-mail para mais detalhes.\",
                type:\"success\"}).then((result)=>{
                    if(result.value){
                        window.location.href=\"index.php\";
                    }
                });
                </script>";
            unset($_SESSION['carrinho']);
        }
    }else{
        header("Location: login-user.php?pagina=finalizar");
    }

?>