<?php
session_start();
if (isset($_SESSION['logado']) && $_SESSION['logado']) {
    require_once("classes/control/FotoControl.class.php");
    $foto = new Foto();
    require_once("menu/menu.php");
    ?>   
        <div id="form_login">
            <form action="userUp.php?id=<?php echo $_SESSION['dadosUser']['id']?>" method="post" enctype="multipart/form-data" id="perfil_user">
            <p id="p_login" class="uk-text-center">Perfil</p>
            <p id="alerta"></p>
            <div class="uk-padding-small">
                <?php 
                    if($foto->verificaSeTem($_SESSION['dadosUser']['id'])){
                ?>
            <div style="width: 50%; margin: 0 auto;">
                <label for="">Imagem Atual</label>
                    <img src="imageUse.php?id=<?php echo $_SESSION['dadosUser']['id']?>" alt="" style="border-radius:50%; background-size:cover; height:200px; width: 200px; padding: 10px;">
            </div>
            <div class="apaI">
                <input type="checkbox" class="uk-checkbox" name="excluir" onclick="imagem(this)"  id="checkImg"  /> <label for="checkImg">excluir imagem</label><br>
            </div>
            <?php }else{?>
                    <h3>Sem imagem para perfil</h3><br>
            <?php } ?>
                <div class="apaI">
                <div class="uk-text-center uk-margin-bottom uk-margin-top">   
                    <div class="uk-margin" id="imagem" uk-margin>    
                        <div uk-form-custom="target:true;">
                                <span>Nova imagem: </span>
                                <input type="file" name="foto_p" id="foto_p">
                                <input type="text" placeholder="Nova imagem" class="uk-input uk-form-width-medium"  disabled/>
                        </div> 
                    </div>
                </div>
                </div>
                <label for="usuario">Identificador do usuario</label><br /><br />
                <div class="uk-inline uk-width-1">
                    <a href="#" class="uk-form-icon" uk-icon="icon: user; ratio: 1.5;"></a>
                    <input type="text" name="user" id="user" class="uk-input" value="<?php echo $_SESSION['dadosUser']['usuario'] ?>" onchange="userP(this, '<?php echo $_SESSION['dadosUser']['usuario']?>');" disabled/>
                </div>
                <span id="erroU" class="erroSp"></span>
                <br /><br />
                <label for="em">Endereço de email:</label><br /><br />
                <div class="uk-inline uk-width-1">
                    <span class="uk-form-icon" uk-icon="icon: mail; ratio: 1.5;"></span>
                    <input type="email" name="email" id="em" value="<?php echo $_SESSION['dadosUser']['email'] ?>" class="uk-input" onchange="emailP(this,'<?php echo $_SESSION['dadosUser']['email'] ?>');" disabled>
                </div>
                <span id="erroEm" class="erroSp"></span>
                <br /><br />
                <div class="apaI">
                <ul>
                    <li>A senha deve conter ao menos 8 caracteres.</li>
                    <li>Ao menos 1 letra(s) maiúscula(s).</li> 
                    <li>Ao menos 1 letra(s) minúscula(s).</li>
                    <li>Ao menos 1 digito(s).</li>
                    <li><em>obs.: Não inicie a senha com número.</em></li>
                </ul>
                <label for="pass">Senha</label>
                <div class="uk-inline uk-width-1">
                    <a href="#"  class="uk-form-icon" uk-icon="icon: lock; ratio: 1.5;"></a>
                    <input type="password" name="senha" onchange="senhaU(this);" id="pass" placeholder="Exemplo5" class="uk-input">
                </div>
                <span id="erroS" class="erroSp"></span>
                <br /><br />
                <label for="passR">Repita a senha</label>
                <div class="uk-inline uk-width-1">
                    <a href="#"  class="uk-form-icon" uk-icon="icon: lock; ratio: 1.5;"></a>
                    <input type="password" name="senha" id="passR" placeholder="Exemplo5" class="uk-input">
                </div>
                <span id="erroSR" class="erroSp"></span>
                <br /><br />
                </div>
                <input type="submit" value="editar perfil" id="perfil"  onclick="return perfi(this, '<?php echo $_SESSION['dadosUser']['id']?>');" class="uk-button uk-button-primary"/>
                </div>
            </form>
        </div>      
    <?php
    require_once("menu/rodape.php");
} else {
    header("Location: index.php");
}
?>