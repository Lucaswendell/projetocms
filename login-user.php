<?php
session_start();
if(isset($_SESSION['logado'])){
    header("Location: index.php");
}else{
    require_once("menu/menu.php");
?>
<div id="tudo">
<div id="form_login">
    <div>
        <form action="#" id="form" method="post">
            <p id="p_login" class="uk-text-center">Login</p> 
            <p id="alert"></p>
            <div class="uk-padding-small">
            <label for="usuario">Email:</label>
            <br /><br />
            <div class="uk-inline uk-width-1">
                <span class="uk-form-icon" uk-icon="icon: user; ratio: 1.5;"></span>
                <input type="text" name="email" id="usuario" placeholder="Seu email" class="uk-input">
            </div>
            <br /><br />
            <label for="senha">Senha:</label>
            <br /><br />
            <div class="uk-inline uk-width-1">
                <span class="uk-form-icon" uk-icon="icon: lock; ratio: 1.5;"></span>
                <input type="password" name="senha" id="senha" placeholder="Sua senha" class="uk-input">
            </div>
            <br /><br />
            <input type="submit" value="Logar" id="lg" class="uk-button uk-button-primary" onclick="return verificarUser(this);"/>
        </div>
        </form>
    </div>
</div>
</div>
<?php
require_once("menu/rodape.php");
if(isset($_SESSION['alerta']['certo'])){
echo
    " <script>
        UIkit.notification(\"{$_SESSION['alerta']['certo']}\");
    </script>
    ";
}
?>

<?php }?>