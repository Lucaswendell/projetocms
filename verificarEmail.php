<?php
    require_once("classes/control/UserControl.class.php");
    $user = new UserControl();
    $regex = "/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/";
    if(!preg_match($regex, $_GET['email'])){
        echo "Email inválido";
    }else if($user->checkEmail($_GET['email'])){
        echo "Email já cadastrado";
    }else{
        echo "";
    }
?>