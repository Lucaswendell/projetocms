<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nome a ser decidido</title>
    <link rel="stylesheet" href="css/uikit.min.css">
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="tudo">
<!--Preloader-->
<div id="carregando"></div>
<!--Menu responsivo-->
<div id="teste" uk-offcanvas="overlay:true ">
        <div class="uk-offcanvas-bar uk-flex uk-flex-column" id="menuRes">
            <button class="uk-offcanvas-close uk-close-large" uk-close></button>
            
            <ul class="uk-nav uk-nav-primary menor">
            <br/><br/>    
            <li ><a href="index.php" class="menu">nome a ser decidido</a></li>
                <li><a href="index.php" class="menu"><span uk-icon="icon: home; ratio: 1.5"></span> Home</a></li>
                <li class="uk-parent">
                    <?php
                        if(!isset($_SESSION['logado'])){
                            echo "
                            <a href=\"#\" onclick=\"return login();\" class=\"menu\"><span uk-icon=\"icon: sign-in; ratio: 1.5;\"></span>entrar</a>
                            ";
                        }else{
                            echo "
                            <a href=\"#\" class=\"menu\">
                            <span uk-icon=\"icon:user; ratio: 1.5;\"></span>{$_SESSION['dadosUser']['usuario']}</a>
                            <ul class=\"uk-nav-sub uk-list\">
                            <li>
                                <a href=\"#\" onclick=\"sair();\" class=\"menu\"><span uk-icon=\"icon: sign-out; ratio: 1.5;\"></span>Sair</a>
                            </li>
                            <li>
                                <a href=\"user.php\"  class=\"menu\"><span uk-icon=\"icon: settings; ratio: 1.5;\"></span>Perfil</a>
                            </li>
                        </ul>";
                        }
                    ?>
                    
                </li>
                <li class="uk-parent" >
                    <a href="#" class="menu">Categoria</a>
                    <ul class="uk-nav-sub uk-list ">
                    <?php
                        require_once("classes/control/ProdutoControl.class.php");
                        $prod = new ControlProd();
                        foreach($prod->selecionaCat() as $cat){
                            echo "
                            <li>
                                <a class=\"menu uk-link\" href=\"categoria.php?categoria={$cat->categoria}\">
                                $cat->categoria
                                </a>
                            </li>";
                        }
                    ?>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#" class="menu"><span uk-icon="icon:cart; ratio: 1.5;"></span> Carrinho</a>
                    <ul class="uk-nav-sub uk-list">
                        <?php
                            if(isset($_SESSION['carrinho'])){
                                echo "<a class=\"menu\" href=\"carrinho.php?acao=visualiza\">Visualizar carrinho</a>";
                            }else{
                                echo "<p>Você não tem itens no carrinho</p>";
                            }
                        
                        ?>
                    </ul>
                </li>
                    <?php
                        if(isset($_SESSION['adm']) && $_SESSION['adm']){
                            echo "
                            <li class=\"uk-nav-header menu\">Administrador</li>
                            <li><a href=\"produtoA.php\" class=\"menu\"><span uk-icon=\"plus-circle\"></span> Adicionar produto</a></li>              <li><a href=\"todosProdutos.php\" class=\"menu\"><span uk-icon=\"icon: pencil; ratio: 1.5;\"></span> Editar produtos</a></li>
                            ";
                        }
                    ?>
            </ul>
        </div>
    </div>
    <!--Menu "normal"-->
    <nav class="uk-navbar-container uk-width-*" uk-navbar>
        <!--Botao responsivo-->
        <div class="uk-navbar-left">
            <a href="#" class="uk-icon responsivo uk-navbar-item" uk-toggle="target: #teste" uk-icon="icon:menu; ratio: 1.5;"></a>
            <a href="index.php" class="uk-navbar-item esconde uk-navbar-logo">Nome a ser decidido</a>
            <ul class="uk-navbar-nav esconde">
                <li><a href="index.php" class="menu"><span uk-icon="icon: home; ratio: 1.5"></span></a></li>
                <?php if (!isset($_SESSION['logado'])) : ?>
                    <li >
                        <a href="#" onclick="return login();">
                            <span class="uk-icon" uk-icon="icon: sign-in; ratio: 1.5;"></span>
                            Entrar
                        </a>
                    </li>
                <?php else : ?>
                    <li>
                        <a href="#" uk-tooltip="title: usuário">
                            <span class="uk-icon" uk-icon="icon: user; ratio: 1.5;"></span>
                            <? echo $_SESSION["dadosUser"]["usuario"] ?>
                        </a>
                            <div class="uk-navbar-dropdown">
                                <ul class="uk-nav uk-navbar-dropdown-nav">
                                    <li>
                                        <a href="#" onclick="sair();" class="preto uk-link">
                                            <span class="uk-icon" uk-icon="icon:sign-out; ratio:1.5;"></span> 
                                            Sair
                                        </a>
                                    </li>
                                    <li>
                                <a href="user.php"  class="preto uk-link"><span uk-icon="icon: settings; ratio: 1.5;"></span>Perfil</a>
                            </li>
                                    </ul>
                                </div>
                        </li>
                <?php endif; ?>   
                        <li>
                        <a href="#"><span uk-icon="icon: cart; ratio: 1.5"></span>carrinho</a>
                                <div class="uk-navbar-dropdown">
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                        <?php
                                            if(isset($_SESSION['carrinho'])){
                                                echo "<li><a class=\"preto uk-link\"href=\"carrinho.php?acao=visualizar\">visualizar carrinho</a></li>";
                                            }else{
                                                echo "<li>Você não tem produto no carrinho  </li>";
                                            }
                                        ?>
                                    </ul>
                                </div>
                        </li>
                        <li>
                            <a href="#">categoria</a>
                                <div class="uk-navbar-dropdown">
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                <?php
                                    require_once("classes/control/ProdutoControl.class.php");
                                    $prod = new ControlProd();
                                foreach($prod->selecionaCat() as $cat){
                                    echo "
                                        <li>
                                            <a href=\"categoria.php?categoria={$cat->categoria}\" class=\"preto uk-link\">
                                            $cat->categoria
                                            </a>
                                        </li>";
                                }
                                ?>
                                    </ul>
                                </div>
                        </li>
                        <?php if(isset($_SESSION['adm']) && $_SESSION['adm']) : ?>
                        <li>
                            <a href="#">Administrador</a>                                
                            <div class="uk-navbar-dropdown">
                                <ul class="uk-nav uk-navbar-dropdown-nav">
                                        <li>
                                            <a href="produtoA.php" class="preto uk-link">
                                                <span class="uk-icon" uk-icon="icon:plus-circle; "></span> Adicionar produto
                                            </a>
                                        </li>
                                        <li><a href="todosProdutos.php" class="preto uk-link"><span uk-icon="icon: pencil; ratio: 1.5;"></span> Editar produtos</a></li>
                                    </ul>
                            </div>
                        </li>
                            <?php endif; ?>
                                
                    </ul>
        </div>
        <div class="nav-overlay uk-navbar-right uk-margin-right responsivo">
                <a href="#" class="uk-navbar-toggle" uk-search-icon uk-toggle="target: .nav-overlay; animation: uk-animation-fade"></a>
        </div>
        <div class="nav-overlay uk-navbar-left uk-flex-1" hidden="">
                <div class="uk-navbar-item uk-width-expand">
                    <form action="pesquisa.php" class="uk-search uk-search-navbar uk-width-1-1" method="post">
                    <input type="search" id="pesquisa" name="pesquisar" class="uk-search-input" placeholder="pesquisar" autofocus/>
                    </form>
                </div>
                <a href="#" class="uk-navbar-toggle" uk-close="" uk-toggle="target:.nav-overlay; animation: uk-animation-fade;"></a>
        </div>
        <div class="uk-navbar-right uk-margin-right esconde">
                <div class="uk-inline">
                    <form action="pesquisa.php" method="post">
                        <button style="color:#ccc !important;" class="uk-form-icon uk-form-icon-flip uk-" uk-icon="icon: search;"></button>    
                        <input type="search" name="pesquisar" class="uk-input" placeholder="pesquisar"/>
                    </form>
                </div>
        </div>
    </nav>
    <div id="cF">