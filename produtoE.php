<?php
    require_once("classes/control/ProdutoControl.class.php");
    require_once("classes/model/Produto.class.php");
    require_once("classes/control/FotoControl.class.php");
    require_once("classes/model/Foto.class.php");
    require_once("classes/control/VideoControl.class.php");
    require_once("classes/model/Video.class.php");
    $produto = new ControlProd();
    $prodM = new Produto();
    $fotoM = new FotoM();
    $fotoC = new Foto();
    $videoM = new videoM();
    $videoC = new video();
    $fotoM->setTipo($_FILES['foto_p']['type']);
    if(!$fotoM->setFoto($_FILES['foto_p']['tmp_name'])){
        $erro = array("codigo"=>0, "mensagem"=> "Coloque uma imagem valida (JPEG, JPG, PNG)");
        echo json_encode($erro);
        exit();
    }else{
        $fotoM->setNome($_POST['nome_p']);
    }
    $videoM->setTipo($_FILES['video']['type']);
    if(!$videoM->setVideo($_FILES['video']['tmp_name'])){
        $erro = array("codigo"=>0, "mensagem"=> "Coloque um video valido (MP3, OGG)");
        echo json_encode($erro);
        exit();
    }else{
        $videoM->setNome($_POST['nome_p']);
    }
    $prodM->setNome_produto($_POST['nome_p']);
    $prodM->setCategoria($_POST['categoria']);
    $prodM->setValor($_POST['valor']);
    $prodM->setId($_POST['id']);
    $prodM->setDescricao($_POST['descricao']);
    if ($produto->atualiza($prodM)){
        if($fotoC->atualizaFoto($fotoM, $prodM->getId())){
            if($videoC->atualizaVideo($videoM, $prodM->getId())){
                $erro = array("codigo"=>1, "mensagem"=> "produto editado com sucesso");
                echo json_encode($erro);
                exit();
            }else{
                $erro = array("codigo"=>1, "mensagem"=> "produto editado com sucesso");
                echo json_encode($erro);
                exit();
            }
        }else{
            $erro = array("codigo"=>0, "mensagem"=> "tivemos um erro para atualizar a imagem");
            echo json_encode($erro);
            exit();
        }
    } else {
        $erro = array("codigo"=>0, "mensagem"=> "tivemos um erro para atualizar o produto");
        echo json_encode($erro);
        exit();
    }
?>