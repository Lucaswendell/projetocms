<?php 
$pagina = isset($_GET['pag']) ? $_GET['pag'] : 1;
setcookie("pag", $pagina, time() + 8400);
session_start();
require_once("menu/menu.php");
?>  
            <h2 class="uk-text-center">Confira alguns produtos</h2>   
            <div class="divCen uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid id="conteudo">
                         
            </div>
<br />
<?php 
$cookie = (isset($_COOKIE['total'])) ? $_COOKIE['total'] : 6;
$qtdPaginas = ceil($cookie / 6);
$voltar = ($pagina > 1) ? $pagina - 1 : 1;
$proxima = ($pagina < $qtdPaginas) ? $pagina + 1 : $qtdPaginas;
echo "<ul class=\"uk-pagination uk-flex-center\">
<li><a href=\"?pag=$voltar\"><span uk-pagination-previous></span>Voltar</a></li>";
for ($i = 1; $i <= $qtdPaginas; $i++) {
    if ($i == $pagina) {
        echo "<li class=\"uk-active\"><span>$i</span></li>";
    } else {
        echo "<li><a href=?pag=$i>$i</a></li>";
    }

}
require_once("classes/control/ProdutoControl.class.php");
echo "
<li><a href=\"?pag=$proxima\">Proxima<span uk-pagination-next></span></a></li>
</ul>";
require_once("menu/rodape.php");
?>
