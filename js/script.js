//preload
$(window).on("load", function () {
    //"esconde" o preloader
    $("#carregando").fadeOut("slow");
});
function carrinho(id, a) {
    $.ajax({
        url: "carrinho.php?id=" + id + "&acao=adicionar",
        method: "post",
        beforeSend: function () {
            carregando(a);
        },
        success: function () {
            location.reload();
        }
    });
}
/* //'converter float para moeda'
function valor(qtd, valor){
    if(qtd.value == 0){
        document.getElementById('valor').innerHTML = (valor).toLocaleString('pt-BR',{
            style: 'currency',
            currency: "BRL"
        });
    }else{
        var total = qtd.value * valor;
        document.getElementById('valor').innerHTML = "Valor total: "+(total).toLocaleString('pt-BR',{
            style: 'currency',
            currency: "BRL"
        });
    }
} */
function perfi(botao, id) {
    if (botao.value == "editar perfil") {
        botao.value = "salvar alterações";
        var apa = document.getElementsByClassName("apaI");
        var input = document.getElementsByTagName("input");
        for (var i = 1; i < input.length; i++) {
            input[i].removeAttribute("disabled", "");
        }
        for (var i = 0; i < apa.length; i++) {
            apa[i].setAttribute("style", "display:block;");
        }
        return false;
    } else if (botao.value == "salvar alterações") {
        let senha = document.getElementById("pass").value;
        let senhaR = document.getElementById("passR").value;
        let alerta = document.getElementById("erroSR");
        let spans = document.getElementsByClassName("erroSp");
        let excluir = document.getElementById("checkImg");
        let erros = 0;
        for (var i = 0; i < spans.length; i++) {
            if (spans[i].innerHTML != "") {
                erros++;
            }
        }
        if (senha != "" && senhaR != "" && senha != senhaR) {
            alerta.setAttribute("style", "color:#f0506e;")
            alerta.innerHTML = "Senhas diferentes";
            erros++;
        } else {
            alerta.innerHTML = "";
        }
        if (erros > 0) {
            return false;
        } else {
            alerta.setAttribute("class", "");
            var data = new FormData($("#perfil_user")[0]);
            if (excluir != null) {
                if (excluir.checked == true) {
                    $.ajax({
                        url: "deletarUserFoto.php?id=" + id,
                        type: "POST",
                        dataType: 'json',
                        success: function (resposta) {
                            if (resposta.codigo == 0) {
                                swal("", resposta.mensagem, "error");
                            }
                        }
                    });
                }
            }
            $.ajax({
                url: "userUp.php?id=" + id,
                data: data,
                type: "POST",
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function () {
                    botao.value = "salvando...";
                },
                success: function (resposta) {
                    if (resposta.codigo == "0") {
                        botao.value = "salvar alterações";
                        swal({
                            text: resposta.mensagem,
                            type: "error"
                        })
                    } else {
                        botao.value = "editar perfil";
                        var apa = document.getElementsByClassName("apaI");
                        var input = document.getElementsByTagName("input");
                        for (var i = 1; i < input.length; i++) {
                            input[i].setAttribute("disabled", "");
                        }
                        for (var i = 0; i < apa.length; i++) {
                            apa[i].setAttribute("style", "display:none;");
                        }
                        swal({
                            text: resposta.mensagem,
                            type: "success"
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                }
            });
            return false;
        }

    } else {
        botao.value = "editar perfil";
        return false;
    }
}
//sair
function sair() {
    $.ajax({ //logout
        url: "login.php?acao=sair",
        dataType: "json",
        success: function (response) {
            if (response.code == "1") {
                location.reload();
            } else {
                swal({
                    text: "erro ao sair",
                    type: "error"
                });
            }
        }
    });
}
//verificar login
function verificarUser() {
    let usuario = document.getElementById("usuario").value;
    let senha = document.getElementById("senha").value;
    let alert = document.getElementById("alert");
    let erros = 0;
    //verifica se o usuario e senha estão vazios
    if (usuario == "" || senha == "") {
        alert.setAttribute("class", "uk-alert uk-alert-danger");
        alert.innerHTML = "Preencha todos os campos<br>";
        erros++;
    } else {
        alert.innerHTML = "";
    }
    if (erros > 0) {
        return false;
    } else {
        var data = $("#form").serialize();
        alert.setAttribute("class", "");
        $.ajax({
            type: 'POST',
            url: 'logar.php',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $("#log").val("Validando...");
            },
            success: function (response) {
                $("#log").val("Logar");
                if (response.codigo == "0") {
                    alert.setAttribute("class", "uk-alert uk-alert-danger");
                    alert.innerHTML = response.mensagem;
                } else {
                    location.reload();
                }
            }
        });
        return false;
    }
}
//verifica os spans do cadastro 
function span() {
    let span = document.getElementsByClassName("erroSp");
    let erro = 0;
    for (var i = 0; i < span.length; i++) {
        if (span[i].innerHTML != "") {
            erro++;
        }
    }
    return erro == 0;
}
//verifica o cadastro do usuário
function verificarCad() {
    let usuario = document.getElementById("user").value;
    let senha = document.getElementById("pass").value;
    let email = document.getElementById("em").value;
    let alerta = document.getElementById("alerta");
    let erros = 0;
    if (usuario == "" || senha == "" || email == "") {
        alerta.innerHTML = "Preencha todos os campos<br>";
        erros++;
    } else {
        alerta.innerHTML = "";
    }
    if (erros > 0) {
        alerta.setAttribute("class", "uk-alert uk-alert-danger");
        return false;
    } else {
        alerta.setAttribute("class", "");
        return true;
    }
}
//login do usuário
function login() {
    swal({
        html: "<div id=\"form_login_index\"><div><form action=\"logar.php\" id=\"form\" method=\"post\"><p id=\"p_login\" class=\"uk-text-center\">Login</p> <p id=\"alert\"></p><div class=\"uk-padding-small\"><label for=\"usuario\">Email:</label><br /><br /><div class=\"uk-inline uk-width-1\"><span class=\"uk-form-icon\" uk-icon=\"icon: user; ratio: 1.5;\"></span><input type=\"text\" name=\"email\" id=\"usuario\" placeholder=\"Seu email\" class=\"uk-input\"></div><br /><br /><label for=\"senha\">Senha:</label><br /><br /><div class=\"uk-inline uk-width-1\"><span class=\"uk-form-icon\" uk-icon=\"icon: lock; ratio: 1.5;\"></span><input type=\"password\" name=\"senha\" id=\"senha\" placeholder=\"Sua senha\" class=\"uk-input\"></div><br /><br /><input type=\"submit\" value=\"Logar\" class=\"uk-button uk-button-primary\" id=\"log\"  onclick=\"return verificarUser();\"/><a href=\"cadastro.php\" class=\"uk-button\">Cadastre-se</a></div></form></div></div>",
        background: "transparent",
        showConfirmButton: false,
        showCloseButton: true
    });
}
//mascaramento
function mascararEd(id) {
    var ina = new Inputmask("(99,99)|(999,99)|(9.999,99)", { placeholder: "0" });
    ina.mask($("#valor_" + id));
}
function mascarar() {
    var ina = new Inputmask("(99,99)|(999,99)|(9.999,99)", { placeholder: "0" });
    ina.mask($("#valor"));
}
//deletar
function temCerteza(a, id, nome, pag) {
    swal({
        text: "Você tem certeza?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "cancelar",
        cancelButtonColor: '#d33'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "get",
                url: "deletar.php?id=" + id + "&nome=" + nome,
                beforeSend: function () {
                    carregando(a);
                },
                success: function () {
                    if (pag == "index") {
                        produtos();
                        var a = document.getElementsByClassName("link");
                        a.forEach(element => {
                            element.setAttribute("style", "block:none;");
                        });
                    } else if (pag == "todos") {
                        lista();
                        var a = document.getElementsByClassName("link");
                        a.forEach(element => {
                            element.setAttribute("style", "block:none;");
                        });
                    } else {
                        location.reload();
                    }
                }
            });
        }
    });
}
//preenche a barra de progresso 
function procresso(e) {
    if (e.lengthComputable) {
        $("#progress").attr({ value: e.loaded, max: e.total });
    }
}
//requisição para editar
function editar(button, id) {
    let nome_p = document.getElementById("nome_p").value;
    let valor = document.getElementById("valor").value;
    let descricao = tinymce.get("descricao").getContent();
    let checkImg = document.getElementById("checkImg");
    let checkVideo = document.getElementById("checkVid");
    var erro = 0;
    if (nome_p == "" || valor == "" || descricao == "") {
        swal("", "Preencha todos os campos", "error");
        erro++;
    } else {
        erro = 0;
    }
    if (erro > 0) {
        return false;
    } else {
        if (checkImg != null && checkImg.checked) {
            $.ajax({
                url: "deletarFoto.php?id="+id,
                method: "POST",
                success: function (resposta) {
                    if (resposta.codigo == 0) {
                        swal("", resposta.mensagem, "error");
                    }
                }
            });
        }
        if (checkVideo != null && checkVideo.checked) {
            $.ajax({
                url: "deletaVideo.php?id="+id,
                method: "POST",
                success: function () {
                    if (resposta.codigo == 0) {
                        swal("", resposta.mensagem, "error");
                    }
                }
            });
        }
        var data = new FormData($("#form_pr")[0]);
        data.set('descricao', descricao);
        $.ajax({
            type: 'POST',
            url: 'produtoE.php',
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            beforeSend: function () {
                $("#progress").fadeIn("slow");
                button.value = "salvando...";
            },
            xhr: function () { //progresso da requisição
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener("progress", procresso, false);
                }
                return myXhr;
            },
            success: function (response) {
                $("#progress").fadeOut("slow");
                button.value = "salvar alterações"
                if (response.codigo == "0") {
                    swal({
                        text: response.mensagem,
                        type: "error"
                    });
                } else {
                    swal({
                        text: response.mensagem,
                        type: "success"
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                    document.forms[2].reset();
                }
            }
        });
        return false;
    }
}
//requisição adicionar o produto
function add(button) {
    let nome_p = document.getElementById("nome_p").value;
    let valor = document.getElementById("valor").value;
    let categoria = document.getElementById("categoria").value;
    let descricao = tinymce.get("descricao").getContent();
    var erro = 0;
    if (nome_p == "" || valor == "" || descricao == "") {
        swal("", "Preencha todos os campos", "error");
        erro++;
    } else if (categoria == "selecione") {
        swal("", "Selecione uma categoria", "error");
        erro++;
    } else {
        erro = 0;
    }
    if (erro > 0) {
        return false;
    } else {
        var data = new FormData($("#form_pr")[0]);
        data.set("descricao", descricao);
        $.ajax({
            type: 'POST',
            url: 'adicionarProduto.php',
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            beforeSend: function () {
                $("#progress").fadeIn("slow");
                button.value = "enviando...";
            },
            xhr: function () { //progresso da requisição
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener("progress", procresso, false);
                }
                return myXhr;
            },
            success: function (response) {
                $("#progress").fadeOut("slow");
                button.value = "adicionar";
                if (response.codigo == "0") {
                    swal({
                        text: response.mensagem,
                        type: "error"
                    });
                } else {
                    swal({
                        text: response.mensagem,
                        type: "success"
                    });
                    document.forms[2].reset();
                }
            }
        });
        return false;
    }
}
//requisições ajax para validação do perfil
//email
function emailP(em, emA) {
    if (em.value != emA) {
        requisitarArquivo(document.getElementById('erroEm'), 'verificarEmail.php?email=' + em.value);
    } else {
        document.getElementById('erroEm').innerHTML = "";
    }
}
//usuario
function userP(us, userA) {
    if (us.value != userA) {
        requisitarArquivo(document.getElementById('erroU'), 'verificarUser.php?user=' + us.value);
    } else {
        document.getElementById('erroU').innerHTML = "";
    }
}
//requisições ajax para validação do cadastro
//email
function emailU(em) {
    requisitarArquivo(document.getElementById('erroEm'), 'verificarEmail.php?email=' + em.value);
}
//usuario
function usuario(us) {
    requisitarArquivo(document.getElementById('erroU'), 'verificarUser.php?user=' + us.value);
}
//senha
function senhaU(sen) {
    requisitarArquivo(document.getElementById('erroS'), 'verificarSenha.php?senha=' + sen.value);
}
//lista de produtos
function lista() {
    var table = document.getElementById("lista");
    if (table != null) {
        requisitarArquivo(table, "mostrar.php?acao=lista");
    }
}
lista();
//post
function produtos() {
    var div = document.getElementById("conteudo");
    if (div != null) {
        requisitarArquivo(div, "mostrar.php?acao=todos");
    }
}
//mostra todos os produtos
produtos();
//checkImg
function imagem(img) {
    if (img != null && img.checked) {
        document.getElementById("imagem").setAttribute("class", "uk-margin apaI");
    } else {
        document.getElementById("imagem").setAttribute("class", "uk-margin");
    }
}
document.getElementById("checkImg").onclcik = function () {
    if (document.getElementById("checkImg") != null && document.getElementById("checkImg").checked) {
        document.getElementById("imagem").setAttribute("class", "uk-margin apaI");
    } else {
        document.getElementById("imagem").setAttribute("class", "uk-margin");
    }
}
//checkVid
function videoV(vid) {
    if (img != null && vid.checked) {
        document.getElementById("video").setAttribute("class", "uk-margin apaI");
    } else {
        document.getElementById("video").setAttribute("class", "uk-margin");
    }
}
//verifica todos os campos para poder enviar o formulario
document.getElementById("bot").onclick = function (event) {
    if (span() && verificarCad()) {
        document.forms.submit();
    } else {
        event.preventDefault();
    }
};
//olha a senha
document.getElementById("pass0").onclick = function () {
    if (document.getElementById("pass").type == "password") {
        document.getElementById("pass").type = "text";
        document.getElementById("pass0").setAttribute("uk-icon", "icon: unlock; ratio: 1.5;");
    } else {
        document.getElementById("pass").type = "password";
        document.getElementById("pass0").setAttribute("uk-icon", "icon: lock; ratio: 1.5;");
    }
};
//carrinho
document.getElementById("carrinho").onclick = function (e) {
    e.preventDefault();
    var qtd = document.getElementById("number");
    $.ajax({
        url: "carrinho.php?acao=adicionar&id="
    });
};