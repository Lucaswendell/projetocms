<?php
session_start();
$pagina = isset($_GET['pag']) ? $_GET['pag'] : 1;
setcookie("pag", $pagina, time() + 8400);
require_once("classes/control/ProdutoControl.class.php");
$prod = new ControlProd();
require_once("menu/menu.php");
echo "<div id=\"tudo\">";
$pes = (isset($_POST['pesquisar'])) ? $_POST['pesquisar'] : "";
if ($prod->pesquisa($pes)) {
    echo "<h3 class=\"uk-text-center\">Resultado da pesquisa</h3>
        <div class=\"divCen uk-child-width-1-2@s uk-child-width-1-3@m\" uk-grid>";
    foreach ($prod->pesquisa($pes) as $produ) {
        $valor = number_format($produ->getValor(), 2, ",", ".");
        echo "
                        <!--Card do produto-->
                        <div class=\"wrap uk-animation-fade\">
                            <div class=\"card\">
                                <div class=\"imgCad\">
                                    <img  src=\"image.php?id={$produ->getId()}\" style=\"width: 100%;\">
                                </div>
                                <div class=\"container\">
                                    <h3>{$produ->getNome_produto()}</h3>
                                    <p>R$ $valor</p>
                                    <p>";
        if (isset($_SESSION['adm']) && $_SESSION['adm']) {
            echo "
                                    <a href=\"editar.php?id={$produ->getId()}&pag=index\" uk-icon=\"icon: pencil; ratio:1.5;\" uk-tooltip=\"editar\"></a>
                                    <a href=\"#\" onclick=\"temCerteza(this, '{$produ->getId()}', '{$produ->getNome_produto()}', 'index');\" uk-icon=\"icon: trash; ratio:1.5;\" uk-tooltip=\"excluir\"></a>";
        }
        echo "
                                    <a  uk-tooltip=\"adicionar ao carrinho\"  href=\"#\" onclick=\"carrinho({$produ->getId()}, this);\" uk-icon=\"icon: cart; ratio: 1.5;\"></a>
                                    <a uk-tooltip=\"ver detalhes do produto\" href=\"detalhes.php?id={$produ->getId()}\" uk-icon=\"icon: plus-circle; ratio: 1.5;\"></a></p>
                                </div>
                            </div>    
                    </div>";
    }
    echo "</div>";
    $qtdPainas = ceil($_COOKIE['total'] / 6);
    $voltar = ($pagina > 1) ? $pagina - 1 : 1;
    $proxima = ($pagina < $qtdPainas) ? $pagina + 1 : $qtdPainas;
    echo "<ul class=\"uk-pagination uk-flex-center\">
    <li><a href=\"?pag=$voltar\"><span uk-pagination-previous></span>Voltar</a></li>";
    for ($i = 1; $i <= $qtdPainas; $i++) {
        if ($i == $pagina) {
            echo "<li class=\"uk-active\"><span>$i</span></li>";
        } else {
            echo "<li><a href=?pag=$i>$i</a></li>";
        }

    }
    echo "
    <li><a href=\"?pag=$proxima\">Proxima<span uk-pagination-next></span></a></li>
    </ul>";
    echo "<br /><br /><br /><br /><br />";
    require_once("menu/rodape.php");
    echo "</div>";
} else {
    echo "
    <h3 class=\"uk-text-center\">Produto não encontrado</h3> 
    <a class=\"uk-button uk-button-default uk-position-center\" href=\"index.php\"><span uk-icon=\"icon: reply;\">Voltar para home</a>";
    echo "</div>";
    require_once("menu/rodape.php");
}

?>