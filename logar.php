<?php
require_once("classes/model/User.class.php");
require_once("classes/control/UserControl.class.php");
if(isset($_POST['senha']) && isset($_POST['email'])){
    $user = new User();
    $control = new UserControl();
    $user->setSenha($_POST['senha']);
    $user->setEmail($_POST['email']);
    if($control->login($user)){
          $erro = array("codigo"=>1, "mensagem"=>"logado com sucesso");
          echo json_encode($erro);
          exit();
    }else{
        $erro = array("codigo" =>0, "mensagem"=>"Usuario ou senha incorreto");
        echo json_encode($erro);
        exit();
    }
}else{
    $erro = array("codigo" =>0, "mensagem"=>"Preencha todos os campos.");
    echo json_encode($erro);
    exit();
}index
?>