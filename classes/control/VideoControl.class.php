<?php
require_once("ProdutoControl.class.php");
require_once("classes/Conexao.class.php");
class Video
{
    public function verificaSeTem($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT video.video, video.tipo from conteudo join videos on videos.id_conteudo = conteudo.id join video on videos.id_video = video.id WHERE conteudo.id =:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            if ($sel->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function deletaVideo($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $ids = $this->pegaIdVideo($id);
        $del = $pdo->getConexao()->prepare("DELETE FROM video WHERE id=:id");
        $del->bindValue(":id",$ids->id_video);
        if($del->execute()){
            $pdo->__destruct();
            return true;
        }else{
            $pdo->__destruct();
            return false;
        }
    }
        //pega o id pelo id do produto na tabela videos
    private function pegaIdVideo($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT * FROM videos WHERE id_conteudo=:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            if ($sel->rowCount() > 0) {
                $idvideo = $sel->fetch();
                $pdo->__destruct();
                return $idvideo;
            } else {
                return "nao";
            }
        } else {
            return false;
        }
    }
        //atualiza o video
    public function atualizaVideo($video, $id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $produto = new ControlProd();
        $idvideo = $this->pegaIdVideo($id);
        if ($video->getVideo() == "vazio") {
            return true;
        } else if ($idvideo == "nao") {
            $this->adicionaVideo($video);
        } else {
            $videobi = file_get_contents($video->getVideo());
            $sql = "UPDATE video SET video=:video, tipo=:tipo, nome=:nome WHERE id=:id";
            $ins = $pdo->getConexao()->prepare($sql);
            $ins->bindValue(":video", $videobi);
            $ins->bindValue(":tipo", $video->getTipo());
            $ins->bindValue(":id", $idvideo->id_video);
            $ins->bindValue(":nome", $video->getNome());
            if ($ins->execute()) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        }
    }
        //pega o id pelo nome para cadastrar a imagem
    public function pegaVid($vid)
    {
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT id FROM video WHERE nome=:nome");
        $consu->bindValue(":nome", $vid);
        if ($consu->execute()) {
            $resu = $consu->fetch();
            $pdo->__destruct();
            return $resu->id;
        } else {
            $pdo->__destruct();
            return false;
        }

    }
    public function videos($prod)
    {
        $produto = new ControlProd();
        $pdo = new Conexao("classes/conf.ini");
        $idvideo = $this->pegaVid($prod);
        $idproduto = $produto->pegaId($prod);
        $sql = "INSERT INTO videos(id_video, id_conteudo) VALUES (:video, :cont)";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->bindValue(":video", $idvideo);
        $ins->bindValue(":cont", $idproduto);
        if ($ins->execute()) {
            $pdo->__destruct();
            return true;
        } else {
            return false;
        }
    }
    public function adicionaVideo($video)
    {
        if ($video->getVideo() != "vazio") {
            $pdo = new Conexao("classes/conf.ini");
                    //converte a imagem em byte
            $videoBin = file_get_contents($video->getVideo());
            $sql = "INSERT INTO video(video, tipo, nome) VALUES (:video, :tipo, :nome)";
            $ins = $pdo->getConexao()->prepare($sql);
            $ins->bindValue(":video", $videoBin);
            $ins->bindValue(":tipo", $video->getTipo());
            $ins->bindValue(":nome", $video->getNome());
            if ($ins->execute()) {
                if ($this->videos($video->getNome())) {
                    $pdo->__destruct();
                    return true;
                } else {
                    $pdo->__destruct();
                    return false;
                }
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            return true;
        }
    }
}

?>