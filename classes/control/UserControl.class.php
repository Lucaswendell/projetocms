<?php
require_once("classes/model/User.class.php");
require_once("classes/Conexao.class.php");
class UserControl{  
        //cadastro
        public function addUser($user){
            $pdo = new Conexao("classes/conf.ini");
            $sql = "INSERT INTO user(usuario, email, senha) VALUES (:us, :em, :se);";
            $ins = $pdo->getConexao()->prepare($sql);
            $ins->bindValue(":us", $user->getUsuario());
            $ins->bindValue(":em",$user->getEmail());
            $ins->bindValue(":se", $user->getSenha());
            if($ins->execute()){
                $pdo->__destruct();
                return true;
            }else{
                $pdo->__destruct();
                return false;
            }
        }
        public function updateUser($user){
            $pdo = new Conexao("classes/conf.ini");
            if(empty($user->getSenha())){
                $sql = "UPDATE user SET email=:email, usuario=:user WHERE id=:id;";
                $search = $pdo->getConexao()->prepare($sql);
                $search->bindValue(":user", $user->getUsuario());
                $search->bindValue(":email", $user->getEmail());   
            }else{
                $sql = "UPDATE user SET email=:email, senha=:pass, usuario=:user WHERE id=:id;";
                $search = $pdo->getConexao()->prepare($sql);                
                $search->bindValue(":user", $user->getUsuario());
                $search->bindValue(":email", $user->getEmail());            
                $search->bindValue(":pass", $user->getSenha());
            }   
            $search->bindValue(":id", $user->getId());            
            if($search->execute()){
                $_SESSION['dadosUser']['usuario'] = $user->getUsuario();
                $_SESSION['dadosUser']['email'] = $user->getEmail();
                $pdo->__destruct();
                return true;
            }else{
                $pdo->__destruct();
                return false;                
            }
        }
        //faz o login
        public function login($user){
            $pdo = new Conexao("classes/conf.ini");
            $sql = "SELECT * FROM user WHERE email=:user AND senha=:pass";
            $search = $pdo->getConexao()->prepare($sql);
            $search->bindValue(":user", $user->getEmail());
            $search->bindValue(":pass", $user->getSenha());
            $search->execute();
            if($search->rowCount() == 1){
                session_start();
                $valores = $search->fetch();
                $_SESSION['logado'] = true;
                $_SESSION['dadosUser']['usuario'] = $valores->usuario;
                $_SESSION['dadosUser']['email'] = $valores->email;
                $_SESSION['dadosUser']['id'] = $valores->id;
                if($valores->adm == 1){
                    $_SESSION['adm'] = true;
                    setcookie("sim",1);
                }else{
                    $_SESSION['adm'] = false;
                }
                $pdo->__destruct();
                return true;
            }else{
                $pdo->__destruct();
                return false;
            }
        }
        //verifica se o usuario ou email existe
        public function checkEmail($email){
            $pdo = new Conexao("classes/conf.ini");
            $sql = "SELECT * FROM user WHERE email=:email";
            $search = $pdo->getConexao()->prepare($sql);
            $search->bindValue(":email", $email);
            $search->execute();
            if($search->rowCount() > 0){
                $pdo->__destruct();
                return true;
            }else{
                $pdo->__destruct();
                return false;
            }
        }
        public function pegaId($usuario){
            $pdo = new Conexao("classes/conf.ini");
            $sql = "SELECT id FROM user WHERE usuario=:user";
            $search = $pdo->getConexao()->prepare($sql);
            $search->bindValue(":user", $usuario);
            if($search->execute()){
                $pdo->__destruct();
                $id = $search->fetch();
                return $id->id;
            }else{
                $pdo->__destruct();
                return false;
            }
        }
        public function checkUser($user){
            $pdo = new Conexao("classes/conf.ini");
            $sql = "SELECT * FROM user WHERE usuario=:pass";
            $search = $pdo->getConexao()->prepare($sql);
            $search->bindValue(":pass", $user);
            $search->execute();
            if($search->rowCount() > 0){
                $pdo->__destruct();
                return true;
            }else{
                $pdo->__destruct();
                return false;
            }
        }
    }
?>