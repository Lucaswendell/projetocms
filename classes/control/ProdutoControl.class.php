<?php
require_once("classes/model/Produto.class.php");
require_once("classes/Conexao.class.php");
class ControlProd{
    public function atualiza($prod){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("UPDATE conteudo SET nome_produto=:nome, categoria=:cat, valor=:val, descricao=:descr WHERE id=:id");
        $cate = $this->pegaCat($prod->getCategoria());
        $consu->bindValue(":id", $prod->getId());        
        $consu->bindValue(":cat", $cate->id);
        $consu->bindValue(":nome", $prod->getNome_produto());
        $consu->bindValue(":val", $prod->getValor());
        $consu->bindValue(":descr", $prod->getDescricao());
        if($consu->execute()){ 
            $pdo->__destruct(); 
            return true;
        }
    }
    public function pesquisa($q){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT * FROM conteudo WHERE nome_produto LIKE :cat");
        $consu->bindValue(":cat","%$q%");
        $consu->execute();
        $lista = [];
        if($consu->rowCount() > 0){
            foreach($consu->fetchAll() as $item){
                $prod = new Produto();
                $prod->setNome_produto($item->nome_produto);
                $prod->setId($item->id);
                $prod->setValor($item->valor);
                $prod->setCategoria($item->categoria);
                $prod->setDescricao($item->descricao);
                array_push($lista, $prod);
            }
            $pdo->__destruct();
            return $lista;
        }else{
            $pdo->__destruct();
            return false;
        }
        
    }
    //apaga o produto
    public function deletar($id, $nome){
        $pdo = new Conexao("classes/conf.ini");
        $delFoto = $pdo->getConexao()->prepare("DELETE FROM foto WHERE nome=:nom");
        $delFoto->bindValue(":nom",$nome);
        if($delFoto->execute()){
            $delV = $pdo->getConexao()->prepare("DELETE FROM video WHERE nome=:nom");
            $delV->bindValue(":nom",$nome);
            if($delV->execute()){
                $delCont = $pdo->getConexao()->prepare("DELETE FROM conteudo WHERE id=:id");
                $delCont->bindValue(":id",$id);
                if($delCont->execute()){
                    $pdo->__destruct();
                    return true;
                }else{
                    $pdo->__destruct();
                    return false;
                }
            }else{
                $pdo->__destruct();
                return false;
            }
        }else{
            $pdo->__destruct();
            return false;
        }
    }
    //pega o produto pelo id 
    public function pegaProd($prod){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT * FROM conteudo WHERE id=:id");
        $consu->bindValue(":id",$prod);
        if($consu->execute()){
            if($consu->rowCount() > 0){
                $item = $consu->fetch();
                $categoria = $this->pegaIdCat($item->categoria);
                $pro = new Produto();
                $pro->setId($item->id);
                $pro->setCategoria($categoria->categoria);
                $pro->setValor($item->valor);
                $pro->setDescricao($item->descricao);
                $pro->setNome_produto($item->nome_produto);
                $pdo->__destruct();
                return $pro;
            }else{
                $pdo->__destruct();
                return false;
            }
        }else{
            $pdo->__destruct();
            return false;
        }
        
    }
    //pega o id do produto
    public function pegaId($prod){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT id FROM conteudo WHERE nome_produto=:cat");
        $consu->bindValue(":cat",$prod);
        $consu->execute();
        $item = $consu->fetch();
        return $item->id;
    }
    //pega o id da categoria atravez da coluna categoria para adicionar no produto
    private function pegaCat($categoria){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT id FROM categoria WHERE categoria=:cat");
        $consu->bindValue(":cat",$categoria);
        $consu->execute();
        $resu = $consu->fetch();
        $pdo->__destruct();
        return $resu;       
    }
    //pega a categoria atravez do id
    public function pegaIdCat($id){
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT categoria FROM categoria WHERE id=:cat");
        $consu->bindValue(":cat",$id);
        $consu->execute();
        $resu = $consu->fetch();
        $pdo->__destruct();
        return $resu;       
    }
    //verifica se o produto já está cadastrado
    public function verificaProd($nome){
        $pdo = new Conexao("classes/conf.ini");
        $sql = "SELECT * FROM conteudo WHERE nome_produto=:nome";
        $busca = $pdo->getConexao()->prepare($sql);
        $busca->bindValue(":nome",strtolower($nome));
        $busca->execute();
        if($busca->rowCount() > 0){
            $pdo->__destruct();
            return true;
        }else{
            $pdo->__destruct();
            return false;
        }
    }
    //adiciona produto
    public function adicionaProd($prod){
        $pdo = new Conexao("classes/conf.ini");
        $ins = $pdo->getConexao()->prepare("INSERT INTO conteudo(nome_produto, categoria, valor, descricao) VALUES (:nom, :categoria, :valor, :descr);");
        $idCate = $this->pegaCat($prod->getCategoria());
        $ins->bindValue(":nom", $prod->getNome_produto());
        $ins->bindValue(":categoria", $idCate->id);
        $ins->bindValue(":valor",$prod->getValor());
        $ins->bindValue(":descr",$prod->getDescricao());
        if($ins->execute()){
            $pdo->__destruct();
            return true;
        }else{
            $pdo->__destruct();
            return false;
        }
    }
    //total de produtos
    public function total($q = "total"){
        $pdo = new Conexao("classes/conf.ini");
        $sql = "SELECT count(id) as qtd FROM conteudo";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->execute();
        $tot = $ins->fetch();
        if($q == "total"){
            setcookie("total",$tot->qtd, time()+8400);
        }else{
            return $tot->qtd;
        }
        
    }
    //seleciona todos os produtos
    public function selecionaProd($paginaAtual){
        $this->total();
        //calcula o inicio da visualização com base na pagina atual
        $inicio = (6 * $paginaAtual) - 6;
        //conexao com o banco
        $pdo = new Conexao("classes/conf.ini");
        $sql = "SELECT * FROM conteudo LIMIT $inicio, 6";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->execute();
        $lista = [];
        foreach($ins->fetchAll() as $prod){
            $produto = new Produto();
            $produto->setNome_produto($prod->nome_produto);
            $produto->setId($prod->id);
            $produto->setDescricao($prod->descricao);
            $produto->setValor($prod->valor);
            $produto->setCategoria($prod->categoria);
            array_push($lista, $produto);
        }
        $pdo->__destruct();
        return $lista;
    }
    //pega a categoria sem criteria
    public function selecionaCat(){
        $pdo = new Conexao("classes/conf.ini");
        $sql = "SELECT * FROM categoria";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->execute();
        $lista = [];
        foreach($ins->fetchAll() as $item){
            array_push($lista, $item);
        }
        $pdo->__destruct();
        return $lista;
    }
    
    public function totalCat($cat){
        $pdo = new Conexao("classes/conf.ini");
        $sql = "SELECT count(conteudo.id) as total FROM conteudo INNER JOIN categoria ON conteudo.categoria = categoria.id WHERE categoria.categoria=:cat";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->bindValue(":cat",$cat);
        $ins->execute();
        $tot = $ins->fetch();
        setcookie("total",$tot->total);
    }
    //seleciona o produto pela categoria
    public function selecionarProdCat($cat, $paginaAtual){
        $inicio = (6 * $paginaAtual) - 6;
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT conteudo.id, conteudo.nome_produto, conteudo.valor, categoria.categoria, conteudo.descricao FROM conteudo INNER JOIN categoria ON conteudo.categoria = categoria.id WHERE categoria.categoria=:cat LIMIT $inicio, 6;");
        $consu->bindValue(":cat",$cat);
        $this->totalCat($cat);
        $consu->execute();
        $lista = []; 
        if($consu->rowCount() > 0){
            foreach($consu->fetchAll() as $item){
                $prod = new Produto();
                $prod->setNome_produto($item->nome_produto);
                $prod->setId($item->id);
                $prod->setValor($item->valor);
                $prod->setCategoria($item->categoria);
                array_push($lista, $prod);
            }
            $pdo->__destruct();
            return $lista;
        }else{
            $pdo->__destruct();
            return false;
        }
        
    }
}
?>