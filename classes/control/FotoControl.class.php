<?php
require_once("ProdutoControl.class.php");
require_once("classes/Conexao.class.php");
require_once("classes/control/UserControl.class.php");
class Foto
{
    public function deletarFotoProd($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $idFoto = $this->pegaIdFoto($id);
        $up = $pdo->getConexao()->prepare("UPDATE fotos SET id_foto=1 WHERE id=:id");
        $up->bindValue(":id", $idFoto->id);
        if ($up->execute()) {
            $del = $pdo->getConexao()->prepare('DELETE FROM foto WHERE id=:id');
            $del->bindValue(":id", $idFoto->id_foto);
            if ($del->execute()) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        }
    }
    public function deletarFotoUser($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $idFoto = $this->pegaIdFotoUser($id);
        if ($idFoto) {
            $sql = "DELETE FROM fotoUser WHERE id=:id";
            $del = $pdo->getConexao()->prepare($sql);
            $del->bindValue(":id", $idFoto->id);
            if ($del->execute()) {
                $sql = "DELETE FROM foto WHERE id=:id";
                $del = $pdo->getConexao()->prepare($sql);
                $del->bindValue(":id", $idFoto->id_foto);
                if ($del->execute()) {
                    $pdo->__destruct();
                    return true;
                } else {
                    $pdo->__destruct();
                    return false;
                }
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            $pdo->__destruct();
            return false;
        }
    }
    public function adicionaFotosUser($foto, $nome)
    {
        $pdo = new Conexao("classes/conf.ini");
        $user = new UserControl();
        $idUser = $user->pegaId($nome);
        $idFoto = $this->pegaFot($foto->getNome());
        $sql = "INSERT INTO fotoUser(id_user, id_foto) VALUES (:idU, :idF)";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->bindValue(":idU", $idUser);
        $ins->bindValue(":idF", $idFoto);
        if ($ins->execute()) {
            $pdo->__destruct();
            return true;
        } else {
            $pdo->__destruct();
            return false;
        }
    }
    public function atualizaFotoUser($id, $foto)
    {
        if ($foto->getFoto() != 1) {
            $pdo = new Conexao("classes/conf.ini");
            $fotobin = file_get_contents($foto->getFoto());
            $sql = "UPDATE foto SET foto=:foto,  tipo=:tipo, nome=:nome WHERE id=:id;";
            $idfoto = $this->pegaIdFotoUser($id)->id_foto;
            $up = $pdo->getConexao()->prepare($sql);
            $up->bindValue(":foto", $fotobin);
            $up->bindValue(":tipo", $foto->getTipo());
            $up->bindValue(":nome", $foto->getNome());
            $up->bindValue(":id", $idfoto);
            if ($up->execute()) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            return true;
        }
    }
    public function adicionaFotoUser($foto, $id)
    {
        if ($foto->getFoto() != 1) {
            if (!$this->verificaSeTem($id)) {
                $pdo = new Conexao("classes/conf.ini");
                $fotobin = file_get_contents($foto->getFoto());
                $sql = "INSERT INTO foto (foto, tipo, nome)VALUES (:foto,:tipo, :nome)";
                $ins = $pdo->getConexao()->prepare($sql);
                $ins->bindValue(":foto", $fotobin);
                $ins->bindValue(":tipo", $foto->getTipo());
                $ins->bindValue(":nome", $foto->getNome());
                if ($ins->execute()) {
                    if ($this->adicionaFotosUser($foto, $foto->getNome())) {
                        $pdo->__destruct();
                        return true;
                    }
                } else {
                    $pdo->__destruct();
                    return false;
                }
            } else {
                if ($this->atualizaFotoUser($id, $foto)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }
        //pega o id da foto pelo id do conteudo
    public function pegaIdFoto($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT * FROM fotos WHERE id_conteudo=:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            $idFoto = $sel->fetch();
            $pdo->__destruct();
            return $idFoto;
        } else {
            return false;
        }
    }
    //verifica se tem imagem
    public function verificaSeTemProd($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT foto.foto, foto.tipo from conteudo join fotos on fotos.id_conteudo = conteudo.id join foto on fotos.id_foto = foto.id WHERE conteudo.id =:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            if ($sel->rowCount() > 0) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            $pdo->__destruct();
            return false;
        }
    }
    //verifica se tem foto no usuario
    public function verificaSeTem($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT foto.foto, foto.tipo from user join fotoUser on fotoUser.id_user = user.id join foto on fotoUser.id_foto = foto.id WHERE user.id =:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            if ($sel->rowCount() > 0) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            $pdo->__destruct();
            return false;
        }
    }
        //pega o id da foto pelo id do usuario
    private function pegaIdFotoUser($id)
    {
        $pdo = new Conexao("classes/conf.ini");
        $sel = $pdo->getConexao()->prepare("SELECT * FROM fotoUser WHERE id_user=:id");
        $sel->bindValue(":id", $id);
        if ($sel->execute()) {
            $idFoto = $sel->fetch();
            $pdo->__destruct();
            return $idFoto;
        } else {
            return false;
        }
    }
    public function atualizaFoto($foto, $id)
    {
        if (empty($foto->getFoto())) {
            return true;
        } else if ($foto->getFoto() == 1) {
            return true;
        } else {
            $pdo = new Conexao("classes/conf.ini");
            $produto = new ControlProd();
            $fotobi = file_get_contents($foto->getFoto());
            $sql = "UPDATE foto SET foto=:foto, tipo=:tipo, nome=:nome WHERE id=:id";
            $idfoto = $this->pegaIdFoto($id);
            if ($idfoto->id_foto == 1) {
                if ($this->adicionaFotoNova($foto, $idfoto)) {
                    $pdo->__destruct();
                    return true;
                } else {
                    $pdo->__destruct();
                    return false;
                }
            } else {
                $ins = $pdo->getConexao()->prepare($sql);
                $ins->bindValue(":foto", $fotobi);
                $ins->bindValue(":tipo", $foto->getTipo());
                $ins->bindValue(":id", $idfoto->id_foto);
                $ins->bindValue(":nome", $foto->getNome());
                if ($ins->execute()) {
                    $pdo->__destruct();
                    return true;
                } else {
                    $pdo->__destruct();
                    return false;
                }
            }
        }
    }
    public function adicionaFotoPadrao($prod)
    {
        $pdo = new Conexao("classes/conf.ini");
        $produto = new ControlProd();
        $idproduto = $produto->pegaId($prod);
        $ins = $pdo->getConexao()->prepare("INSERT INTO fotos(id_conteudo) VALUES (:id_prod);");
        $ins->bindValue(":id_prod", $idproduto);
        if ($ins->execute()) {
            $pdo->__destruct();
            return true;
        }
    }
        //adiciona uma nova foto caso o produto esteja com a foto padrao
    public function adicionaFotoNova($foto, $id)
    {
        $pdo = new Conexao("classes/conf.ini");
            //converte a imagem em byte
        $fotobi = file_get_contents($foto->getFoto());
        $sql = "INSERT INTO foto(foto, tipo, nome) VALUES (:foto, :tipo, :nome)";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->bindValue(":foto", $fotobi);
        $ins->bindValue(":tipo", $foto->getTipo());
        $ins->bindValue(":nome", $foto->getNome());
        if ($ins->execute()) {
            if ($this->atualizaFotos($id, $foto->getNome())) {
                $pdo->__destruct();
                return true;
            } else {
                $pdo->__destruct();
                return false;
            }
        } else {
            $pdo->__destruct();
            return false;
        }
    }
        //atualiza o id da foto para o novo id
    public function atualizaFotos($id, $foto)
    {
        $produto = new ControlProd();
        $pdo = new Conexao("classes/conf.ini");
        $sql = "UPDATE fotos SET id_foto = :foto WHERE id = :idtabela";
        $ins = $pdo->getConexao()->prepare($sql);
        $id_foto = $this->pegaFot($foto);
        $ins->bindValue(":foto", $id_foto);
        $ins->bindValue(":idtabela", $id->id);
        if ($ins->execute()) {
            $pdo->__destruct();
            return true;
        } else {
            $pdo->__destruct();
            return false;
        }
    }
        //pega o id pelo nome
    public function pegaFot($fot)
    {
        $pdo = new Conexao("classes/conf.ini");
        $consu = $pdo->getConexao()->prepare("SELECT id FROM foto WHERE nome=:nome");
        $consu->bindValue(":nome", $fot);
        if ($consu->execute()) {
            $resu = $consu->fetch();
            $pdo->__destruct();
            return $resu->id;
        } else {
            $pdo->__destruct();
            return false;
        }
    }
    public function fotos($prod)
    {
        $produto = new ControlProd();
        $pdo = new Conexao("classes/conf.ini");
        $idfoto = $this->pegaFot($prod);
        $idproduto = $produto->pegaId($prod);
        $sql = "INSERT INTO fotos(id_foto, id_conteudo) VALUES (:foto, :cont)";
        $ins = $pdo->getConexao()->prepare($sql);
        $ins->bindValue(":foto", $idfoto);
        $ins->bindValue(":cont", $idproduto);
        if ($ins->execute()) {
            $pdo->__destruct();
            return true;
        }
    }
    public function adicionaFoto($foto)
    {
        if ($foto->getFoto() == 1) {
                //adiciona a foto padrão
            if ($this->adicionaFotoPadrao($foto->getNome())) {
                return true;
            } else {
                return false;
            }
        } else {
            $pdo = new Conexao("classes/conf.ini");
                //converte a imagem em byte
            $fotobi = file_get_contents($foto->getFoto());
            $sql = "INSERT INTO foto(foto, tipo, nome) VALUES (:foto, :tipo, :nome)";
            $ins = $pdo->getConexao()->prepare($sql);
            $ins->bindValue(":foto", $fotobi);
            $ins->bindValue(":tipo", $foto->getTipo());
            $ins->bindValue(":nome", $foto->getNome());
            if ($ins->execute()) {
                if ($this->fotos($foto->getNome())) {
                    $pdo->__destruct();
                    return true;
                } else {
                    $pdo->__destruct();
                    return false;
                }
            } else {
                $pdo->__destruct();
                return false;
            }
        }
    }

}

?>