<?php
    class Produto{
        private $id;    
        private $categoria;
        private $nome_produto;
        private $valor;
        private $descricao;
        
        public function getDescricao(){
            return $this->descricao;
        }
        public function getId(){
            return $this->id;
        }
        public function getCategoria(){
            return $this->categoria;
        }
        public function getNome_produto(){
            return $this->nome_produto;
        }
        public function getValor(){
            return $this->valor;
        }
        public function setCategoria($categoria){
            $this->categoria = (isset($categoria)) ? addslashes(strtolower($categoria)) : NULL;
        }
        public function setNome_produto($nome_produto){
            $this->nome_produto = (isset($nome_produto)) ? addslashes(strtolower($nome_produto)) : NULL;
        }
       
        public function setValor($valor){
            $valor = str_replace(",",".",$valor);
            $valor = explode(".",$valor);
            if(count($valor) == 3){
                $valor = $valor[0].$valor[1].".".$valor[2];
            }else{
                $valor = $valor[0].".".$valor[1];
            }
            $this->valor = (isset($valor)) ? $valor : NULL;
        }
        public function setId($id){
            $this->id = (isset($id)) ? $id : NULL;
        }
        public function setDescricao($d){
            $this->descricao = (isset($d)) ? $d : NULL;
        }
    }
?>