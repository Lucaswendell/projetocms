<?php
    class VideoM{
        private $nome;
        private $video;
        private $tipo;
        public function setNome($nome){
            $this->nome = (isset($nome)) ? $nome : NULL;
        }
        public function setVideo($v){
            if(!empty($v)){
                if(preg_match("/^video\/(mp4|ogg)$/",$this->getTipo())){
                    $this->video = $v;
                    return true;
                }else{
                    return false;
                }
            }else{
              $this->video = "vazio";
              return true;
            }
        }
        public function setTipo($tipo){
            $this->tipo = (isset($tipo)) ? $tipo : NULL;
        }
        public function getTipo(){
            return $this->tipo;
        }
        public function getVideo(){
            return $this->video;
        }
        public function getNome(){
            return $this->nome;
        }
    }

?>