<?php
class FotoM{
    private $nome;
    private $foto;
    private $tipo;
    public function setNome($nome){
        $this->nome = (isset($nome)) ? $nome : NULL;
    }
    public function setFoto($f){
            if(!empty($f)){
                if(preg_match("/^image\/(jpg|jpeg|png)$/", $this->getTipo())){
                    $this->foto = $f;
                    return true;
                }else{
                    return false;
                }
            }else{
                $this->foto = 1;   
                return true;             
            }
            
    }
    public function setTipo($tipo){
        $this->tipo = (isset($tipo)) ? $tipo : NULL;
    }
    public function getTipo(){
        return $this->tipo;
    }
    public function getFoto(){
        return $this->foto;
    }
    public function getNome(){
        return $this->nome;
    }
}

?>