<?php
class User{
    private $id;
    private $usuario;
    private $email;
    private $senha;
    private $adm;
    public function getId(){
        return $this->id;
    }   
    public function getUsuario(){
        return $this->usuario;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getSenha(){
        return $this->senha;
    }
    public function getAdm(){
        return $this->adm;
    }
    public function setId($id){
        $this->id = isset($id) ? addslashes($id) : NULL;
    }
    public function setUsuario($usuario){
        $this->usuario = isset($usuario) ? addslashes($usuario) : NULL;
    }
    public function setEmail($email){
        $this->email = isset($email) ? addslashes(strtolower($email)) : NULL;
    }
    public function setSenha($senha){
        $this->senha = isset($senha) ? addslashes($senha) : NULL;
    }
    public function setAdm($adm){
        $this->adm = isset($adm) ? addslashes($adm) : NULL;
    }
}


?>