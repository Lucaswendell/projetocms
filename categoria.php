<?php
session_start();
require_once("classes/control/ProdutoControl.class.php");
$pagina = isset($_GET['pag']) ? $_GET['pag']:1;
$categoria = (isset($_GET['categoria'])) ? $_GET['categoria'] : NULL;
$prod = new ControlProd();
$produ = $prod->selecionarProdCat($categoria, $pagina);
$qtdPainas = ceil($pagina / 6);
require_once("menu/menu.php");
if(!empty($categoria) && $produ){
    
    echo "<h3 class=\"uk-text-center\">categoria: {$categoria}</h3>";
    echo "<div class=\"divCen uk-child-width-1-2@s uk-child-width-1-3@m\" uk-grid >";
    foreach($produ as $resu){
        $valor=number_format($resu->getValor(), 2,",",".");
        echo 
            "<!--Card do produto-->
            <div class=\"wrap uk-animation-fade\">
                <div class=\"card\">
                    <div class=\"imgCad\">
                        <img  src=\"image.php?id={$resu->getId()}\" style=\"width: 100%;\">
                    </div>
                    <div class=\"container\">
                        <h2>{$resu->getNome_produto()}</h2>
                        <p>R$ $valor</p>
                        <p>";
                    if(isset($_COOKIE['sim']) && $_COOKIE['sim']){
                        echo "
                        <a href=\"editar.php?id={$resu->getId()}&pag=index\" uk-icon=\"icon: pencil; ratio:1.5;\" uk-tooltip=\"editar\"></a>
                        <a href=\"#\" onclick=\"temCerteza(this, '{$resu->getId()}', '{$resu->getNome_produto()}', 'categoria');\" uk-icon=\"icon: trash; ratio:1.5;\" uk-tooltip=\"excluir\"></a>";
                    }
                        echo "<a  uk-tooltip=\"adicionar ao carrinho\"  href=\"#\" onclick=\"carrinho({$resu->getId()}, this);\" uk-icon=\"icon: cart; ratio: 1.5;\"></a>
                        <a uk-tooltip=\"ver detalhes do produto\" href=\"detalhes.php?id={$resu->getId()}\" uk-icon=\"icon: plus-circle; ratio: 1.5;\"></a></p>
                    </div>
                </div>    
        </div>
            ";
                }
    echo "</div>";
$voltar = ($pagina > 1)? $pagina-1 : 1;
$proxima = ($pagina < $qtdPainas) ? $pagina+1:$qtdPainas;    
echo "<ul class=\"uk-pagination uk-flex-center\">
<li><a href=\"?categoria={$categoria}&pag=$voltar\"><span uk-pagination-previous></span>Voltar</a></li>";
for ($i=1; $i <= $qtdPainas; $i++) { 
    if($i == $pagina){
        echo "<li class=\"uk-active\"><span>$i</span></li>";
    }else{
        echo "<li><a href=?categoria={$categoria}&pag=$i>$i</a></li>";
    }
    
}
echo "
<li><a href=\"?categoria={$categoria}&pag=$i\">Proxima<span uk-pagination-next></span></a></li>
</ul>"; 
}else{
    echo "<h3 class=\"uk-text-center\">Nenhum produto foi encontrado nesta categoria</h3>
    <a href=\"index.php\" class=\"uk-button uk-button-default uk-position-center\"><span uk-icon=\"icon:reply;\"></span>voltar para home</a>
    ";
}
require_once("menu/rodape.php");
?>