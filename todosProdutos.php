<?php
session_start();
if (isset($_SESSION['adm']) && $_SESSION['adm'] && $_SESSION['logado'] && isset($_SESSION['logado'])) {
    $pagina = isset($_GET['pag']) ? $_GET['pag'] : 1;
    setcookie("pag", $pagina, time() + 8400);
    $qtdPainas = ceil($_COOKIE['total'] / 5);
    require_once("menu/menu.php");
    ?> 
    <table class="uk-table uk-table-striped uk-overflow-auto" id="table">
        <thead>
            <tr>
                <th>produto</th>
                <th>valor</th>
                <th>imagem</th>
            </tr>
        </thead>
        <tbody id="lista">

        </tbody>
    </table>

<?php
$voltar = ($pagina > 1) ? $pagina - 1 : 1;
$proxima = ($pagina < $qtdPainas) ? $pagina + 1 : $qtdPainas;
echo "<ul class=\"uk-pagination uk-flex-center\">
    <li><a href=\"?pag=$voltar\"><span uk-pagination-previous></span>Voltar</a></li>";
for ($i = 1; $i <= $qtdPainas; $i++) {
    if ($i == $pagina) {
        echo "<li class=\"uk-active\"><span>$i</span></li>";
    } else {
        echo "<li><a href=?pag=$i>$i</a></li>";
    }
}
echo "
    <li><a href=\"?pag=$proxima\">Proxima<span uk-pagination-next></span></a></li>
    </ul> </div><div>";
require_once("menu/rodape.php");
} else {
    header("Location: index.php");
}
?>