<?php
session_start();
if (isset($_SESSION['logado']) && $_SESSION['adm'] == true) {
    require_once("menu/menu.php");
    require_once("classes/control/ProdutoControl.class.php");
    require_once("classes/control/VideoControl.class.php");
    require_once("classes/control/FotoControl.class.php");
    $prod = new ControlProd();
    $prodId = $prod->pegaProd($_GET['id']);
    $video = new Video();
    $foto = new Foto();
    if ($prodId) {
        ?>
    <div id="form_produto">
        <form action="produtoE.php" id="form_pr" method="post" enctype="multipart/form-data">
                <p id="p_login" class="uk-text-center">Editar produto</p> 
                <p id="alert"></p>           
                <div uk-grid>
                    <div class="uk-width-1">
                        <div class="uk-padding-small">
                            <label for="nome_p">Nome do produto:</label>
                            <input type="text" value="<?php echo $prodId->getNome_produto() ?>" name="nome_p" id="nome_p" placeholder="Nome do produto" class="uk-input"><br /><br />
                            <label for="valor">Valor do produto: (R$)</label><br />
                            <input type="text" value="<?php echo $prodId->getValor() ?>" onclick="mascarar();" name="valor" id="valor" placeholder="Valor do produto" class="uk-input"><br /><br />
                            <label for="categoria">Categoria</label>
                            <select name="categoria" id="categoria" class="uk-select">
                                <option onclick="video(this)" value="<?php echo $prodId->getCategoria() ?>"><?php echo $prodId->getCategoria() ?></option>
                                    <?php
                                    foreach ($prod->selecionaCat() as $cat) {
                                        if ($prodId->getCategoria() != $cat->categoria) {
                                            echo "<option onclick=\"video(this)\" value=\"$cat->categoria\">$cat->categoria</option>";
                                        }
                                    }
                                    ?>
                            </select> 
                            <br/><br/>
                            <label for="descricao">Descrição do produto</label>
                            <textarea placeholder="" id="descricao" name="descricao">
                            <?php
                            echo $prodId->getDescricao();
                            ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="uk-width-1" id="img_p">
                        <?php 
                        if ($foto->verificaSeTemProd($prodId->getId())) {
                            echo "<p class=\"uk-text-center\">Imagem atual</p>
                                <div id=\"img\">
                                    <img id=\"foto_e\" src=\"image.php?id={$_GET['id']}\" style=\"width: 100%;height: 100%;\"><br>
                                    ";
                            if ($foto->pegaIdFoto($prodId->getId())->id_foto != 1) {
                                echo "
                                <div id=\"novaImg\">
                                    <input type=\"checkbox\" onclick=\"imagem(this)\" class=\"uk-checkbox\" id=\"checkImg\"/> <label for=\"checkImg\">Excluir imagem</label>
                                </div>";
                            }
                            echo "
                                </div>
                                ";
                        } else {
                            echo "<h3 class=\"uk-text-center\">Sem imagem</h3>";
                        }
                        ?>
                        <br>
                        <div class="uk-text-center uk-margin-bottom uk-margin-top">
                        <div class="uk-margin" id="imagem" uk-margin>    
                            <div uk-form-custom="target:true;">
                                    <span>Nova imagem</span>
                                    <input type="file" name="foto_p" id="foto_pe">
                                    <input type="text" placeholder="selecione a imagem" class="uk-input uk-form-width-medium"  disabled/>
                            </div> 
                        </div>
                        <br>
                        <div>
                            <?php
                            if ($video->verificaSeTem($prodId->getId())) {
                                echo "   
                                <p class=\"uk-text-center\">Vídeo atual</p> 
                                    <div id=\"vid\">
                                        <video controls preload=\"metadata\" poster src=\"video.php?id={$_GET['id']}\" style=\"width: 100%;height:100%;\">
                                        seu navegador nao suporta
                                </video>
                                </div>
                                <input type=\"checkbox\" onclick=\"videoV(this)\" class=\"uk-checkbox\" id=\"checkVid\"/> <label for=\"checkVid\">Excluir vídeo</label>";

                            } else {
                                echo "<h3>Sem video</h3>";
                            }
                            ?>
                            <div class="uk-margin " id="video" uk-margin >
                                <div uk-form-custom="target:true" > 
                                    <span>Novo video: </span>
                                    <input type="file" name="video"  id="video_p"/>
                                    <input type="text" class="uk-input uk-form-width-medium" placeholder="Selecione um video" disabled>
                                </div> 
                            </div>
                        </div>
                        <br />
                        <br />
                            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                            <progress id="progress" class="uk-progress" style="display: none; margin: 0 auto;width: 50%; height: 20px;"></progress><br />
                            <input type="submit"  id="button" value="salvar alterações" class="uk-button uk-button-primary" onclick="return editar(this, <?php echo $_GET['id'] ?>);"> <br><br>
                            <?php
                            if ($_GET['pag'] == "todos") {
                                $url = "todosProdutos.php";
                            } else if ($_GET['pag'] == "detalhes") {
                                $url = "detalhes.php?id={$_GET['id']}";
                            } else {
                                $url = "index.php";
                            }
                            ?>
                            <a href="<?php echo $url ?>" class="uk-button uk-button-default"><span uk-icon="icon:reply;"></span> Voltar</a>
                        </div>
                    </div>
                </div>
            </form>
    </div>
<?php 
} else {
    echo "<h1 class=\"uk-text-center uk-text-langer\">Produto não encontrado</h1>";
}
} else {
    header("Location: index.php");
}
require_once("menu/rodape.php");
echo "<script src='js/tinymce/js/tinymce/tinymce.min.js'></script>
<script>
 tinymce.init({
        selector: \"#descricao\",
        plugins:[\"lists\",\"link\"],
        toolbar: [
            'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright | bullist numlist outdent indent'
        ],
        menubar: false,
        height: 500,
        language: \"pt_BR\"
    });
</script>";
?>
