<?php
session_start();
if (isset($_GET['acao']) && $_GET['acao'] == "adicionar") {
    $idProduto = $_GET['id'];
        //cria o carrinho caso ainda não tena sido criado
    if (!isset($_SESSION['carrinho'])) {
        $_SESSION['carrinho'] = array();
    }
        //verifica se o produto já existe no carrinho
    if (isset($_SESSION['carrinho'][$idProduto])) {
        $_SESSION['carrinho'][$idProduto]++;
    } else {
        $_SESSION['carrinho'][$idProduto] = 1;
    }
}
if (isset($_GET['acao']) && $_GET['acao'] == "remover") {
    $idProduto = $_GET['id'];
    if (isset($_SESSION['carrinho'][$idProduto])) {
        unset($_SESSION['carrinho'][$idProduto]);
    }
}
if (isset($_GET['acao']) && $_GET['acao'] == "up") {
    if (is_array($_POST['prod'])) {
        foreach ($_POST['prod'] as $id => $qtd) {
            if (!empty($qtd) || $qtd != 0) {
                $_SESSION['carrinho'][$id] = $qtd;
            } else {
                unset($_SESSION['carrinho'][$id]);
            }
        }
    }
}
//mostra carrinho
if (isset($_SESSION['carrinho'])) {
    require_once("menu/menu.php");
    $produto = new ControlProd();
    $produtos = array_keys($_SESSION['carrinho']);
    $total = 0;
    echo "
    <div id=\"tudo\">
    <div uk-overflow-auto>
    <table class=\"uk-table uk-table-divider\" id=\"tableCa\">
    <thead>
        <tr>
            <th>Produto</th>
            <th>Quantidade</th>
            <th>Valor unitário</th>
            <th>Subtotal</th>
        </tr>
    </thead>
        <form action=\"?acao=up\" method=\"post\">        
    <tfoot>
        <tr><td><input type=\"submit\" value=\"atualizar carrinho\" class=\"uk-button uk-button-default\"></td></tr>
    </tfoot>
        <tbody>
    ";

    foreach ($produtos as $prod) {
        $pega = $produto->pegaProd($prod);
        $value = $_SESSION['carrinho'][$prod] * $pega->getValor();
        echo "
        <tr style=\"padding: 10px;\">
            <td>{$pega->getNome_produto()}</td>
            <td><input type=\"number\" name=\"prod[{$pega->getId()}]\" class=\"uk-input uk-form-width-xsmall\" value=\"{$_SESSION['carrinho'][$prod]}\"/></td>
            <td>R$ " . number_format($pega->getValor(), 2, ",", ".") . "</td>
            <td>R$ " . number_format($value, "2", ",", ".") . "</td>
            <td><a href=\"?acao=remover&id={$pega->getId()}\" uk-tooltip=\"remover\" uk-icon=\"icon: trash; ratio:1.5;\"></a></td>
        </tr>";
        $total += $value;
    }
        echo "
        </tbody>
        </form>
     </table>
    </div>
    <p class=\"uk-text-center\">Valor total da compra: R$ " . number_format($total, 2, ",", ".") . "
    <br /><a href=\"index.php\">Continuar comprando</a><br/> <a href=\"finalizar.php\">Finalizar compra</a>
    </p>
    </div>";
    require_once("menu/rodape.php");
} else {
    echo "Não há produtos no carrinho.";
}
?>