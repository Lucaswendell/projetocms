<?php
    $senha = $_GET['senha'];
    $regex = "/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/";
    if(strlen($senha) < 8){
        echo "senha com menos de 8 caracteres";
    }else if(!preg_match($regex, $senha)){
        echo "senha invalida";
    }
?>