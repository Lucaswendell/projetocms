<?php
    require_once("classes/control/UserControl.class.php");
    require_once("classes/model/User.class.php");
    session_start();
    try{
        $user = new User();
        $userCont = new UserControl();
        $user->setUsuario($_POST['user']);
        $user->setSenha($_POST['senha']);
        $user->setEmail($_POST['email']);
        if(!$userCont->checkEmail($user->getEmail())){
            if(!$userCont->checkUser($user->getUsuario())){
                if($userCont->addUser($user)){
                    $_SESSION['alerta']['certo'] = "cadastrado com sucesso";
                    header("Location: login-user.php");
                }else{
                    throw new Exception("erro inesperado");
                }
            }else{
                throw new Exception("usuario já cadastrado");
            }
        }else{
            throw new Exception("Email já cadastrado");
        }
    }catch(Exception $e){
        $_SESSION['alerta']['errada'] = $e->getMessage();
        header("Location: cadastro.php");
    }


?>