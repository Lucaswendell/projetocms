<?php
    if(isset($_GET['acao']) && $_GET['acao'] == "todos"){
        require_once("classes/control/ProdutoControl.class.php");
        $prod = new ControlProd();
        $pagina = $_COOKIE['pag'];
        $res = $prod->selecionaProd($_COOKIE['pag']);
            foreach($res as $resu){
                $valor=number_format($resu->getValor(), 2,",",".");
                echo "
                    <!--Card do produto-->
                    <div class=\"wrap\">
                        <div class=\"card\">
                            <div class=\"imgCad\">
                                <img  src=\"image.php?id={$resu->getId()}\" style=\"width: 100%;\">
                            </div>
                            <div class=\"container\">
                                <h3>{$resu->getNome_produto()}</h3>
                                <p>R$ $valor</p>
                                <p>";
                            if(isset($_COOKIE['sim']) && $_COOKIE['sim']){
                                echo "
                                <a href=\"editar.php?id={$resu->getId()}&pag=index\" uk-icon=\"icon: pencil; ratio:1.5;\" uk-tooltip=\"editar\"></a>
                                <a href=\"#\" onclick=\"temCerteza(this, '{$resu->getId()}', '{$resu->getNome_produto()}', 'index');\" uk-icon=\"icon: trash; ratio:1.5;\" uk-tooltip=\"excluir\"></a>";
                            }
                                echo "<a  uk-tooltip=\"adicionar ao carrinho\"  href=\"#\" onclick=\"carrinho({$resu->getId()}, this);\" uk-icon=\"icon: cart; ratio: 1.5;\"></a>
                                <a uk-tooltip=\"ver detalhes do produto\" href=\"detalhes.php?id={$resu->getId()}\" uk-icon=\"icon: plus-circle; ratio: 1.5;\"></a></p>
                            </div>
                        </div>    
                </div>";
            }
    }else if(isset($_GET['acao']) && $_GET['acao'] == "lista"){
        require_once("classes/control/ProdutoControl.class.php");
        $prod = new ControlProd();
        $res = $prod->selecionaProd($_COOKIE['pag']);
        foreach($res as $resu){
            $valor=number_format($resu->getValor(), 2,",",".");
            echo "
            <tr class=\"uk-animation-fade\">
                <td>{$resu->getNome_produto()}</td>
                <td>{$valor}</td>
                <td><img src=\"image.php?id={$resu->getId()}\"></td>
                <td><a href=\"#\" onclick=\"temCerteza(this,'{$resu->getId()}', '{$resu->getNome_produto()}', 'todos');\" uk-icon=\"icon: trash; ratio:1.5;\" uk-tooltip=\"excluir\"></a\>
                <a href=\"editar.php?id={$resu->getId()}&pag=todos\" uk-icon=\"icon: pencil; ratio:1.5;\" uk-tooltip=\"editar\"></a>
                <a uk-tooltip=\"ver detalhes do produto\" href=\"detalhes.php?id={$resu->getId()}\" uk-icon=\"icon: plus-circle; ratio: 1.5;\"></a></p></td>
            </tr>";
        }
    }else{
    }
?>

