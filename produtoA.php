<?php
session_start();
if (isset($_SESSION['logado']) && $_SESSION['adm'] == true) {
    require_once("menu/menu.php");
?>
    <div id="form_produto">
        <form action="adicionarProduto.php" id="form_pr" method="post" enctype="multipart/form-data">
                <p id="p_login" class="uk-text-center">Adicionar produto</p> 
                <p id="alert"></p>           
                <div uk-grid>
                    <div class="uk-width-1">
                        <div class="uk-padding-small">
                            <label for="nome_p">Nome do produto:</label>
                            <input type="text" name="nome_p" id="nome_p" placeholder="Nome do produto" class="uk-input"><br /><br />
                            <label for="valor">Valor do produto: (R$)</label><br />
                            <input type="text" onfocus="mascarar();" name="valor" id="valor" placeholder="Valor do produto" class="uk-input"><br /><br />
                            <label for="categoria">Categoria</label>
                            <select name="categoria" id="categoria" class="uk-select">
                                <option value="selecione">Selecione...</option>
                                    <?php
                                        require_once("classes/control/ProdutoControl.class.php");
                                        $prod = new ControlProd();
                                        foreach($prod->selecionaCat() as $cat){
                                            echo "<option onclick=\"video(this)\" value=\"$cat->categoria\">$cat->categoria</option>";
                                        }  
                                    ?>
                            </select> 
                            <br/><br/>
                            <label for="descricao">Descrição do produto</label>
                            <textarea  placeholder="Digite uma descrição para o produto" id="descricao"></textarea>
                        </div>
                    </div>
                    <div class="uk-width-1" id="img_p">
                        <div class="uk-text-center uk-margin-bottom uk-margin-top">
                        <div class="uk-margin" uk-margin>    
                            <div uk-form-custom="target:true;">
                                    <span>Selecione uma imagem: </span>
                                    <input type="file" name="foto_p" id="foto_p">
                                    <input type="text" placeholder="selecione uma imagem" class="uk-input uk-form-width-medium"  disabled/>
                            </div> 
                        </div>
                        <div class="uk-margin" uk-margin>
                            <div uk-form-custom="target:true;" id="video">
                                <span>Selecione um video: </span>
                                <input type="file" name="video"  id="video_p"/>
                                <input type="text" class="uk-input uk-form-width-medium" placeholder="Selecione um video" disabled>
                            </div> 
                        </div>
                        <br />
                        <br />
                            <progress id="progress" class="uk-progress" style="display: none; margin: 0 auto;width: 50%; height: 20px;"></progress><br />
                            <input type="submit"  onclick="return add(this)" id="button" value="adicionar" class="uk-button uk-button-primary">
                            <a href="index.php" class="uk-button uk-button-default"><span uk-icon="icon:reply;"></span> Voltar</a>
                        </div>
                    </div>
                </div>
            </form>
    </div>
<?php require_once("menu/rodape.php");?>
<script src='js/tinymce/js/tinymce/tinymce.min.js'></script>
<script>
 tinymce.init({
        selector: "#descricao",
        plugins:["lists","link"],
        toolbar: [
            'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright | bullist numlist outdent indent'
        ],
        menubar: false,
        height: 500,
        language: "pt_BR"
    });
</script>
<?php 
} else {
    header("Location: login-user.php");
}
?>